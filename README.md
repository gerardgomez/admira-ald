# ADmira Player Installer for Linux #

This tool allows you to install the ADmira Player Linux in a Debian based distribution.

After the installation, the system will automatically log into a customized lightweight desktop environment which contains just the minimum packages required to install and run the ADmira Player.

We call **Admira Linux Distribution (ALD)** to the result.

![ALD Desktop](fluxbox/ald-desktop.png)

### Requirements ###

  * Supported Hardware: i686, x86_64, RaspberryPi 3, RaspberryPi 3 Model B+, RasperryPi 4 and Raspberry Pi Compute Module 4 (armv7l)
  * Supported OSS: Debian 8.x (Jessie), 9.x (Stretch) or 10.x (Buster), Raspbian Lite (Jessie, Stretch and Buster) and Raspberry Pi OS Lite (Buster)

### How to install ADmira Player ###

Assuming you have the Debian/Raspberry Pi OS already installed, follow the steps below in order to install the ADmira Player in your computer.

**Note**: If you don't know how to install Raspberry Pi OS and/or Debian go to the sections: **Install Raspberry Pi OS Lite ** and **Install Debian** at the end of the document.

  * If you are using Raspberry Pi OS, log into the system using the `pi` user and the password `raspberry` otherwise log using the `root` user created at installation time. Then execute the following command:
    * **In Raspberry Pi OS**: `wget -qO - https://bitbucket.org/admira/admira-ald/raw/master/setup | sudo -E bash -`
    * **In Debian**:   `wget -qO - https://bitbucket.org/admira/admira-ald/raw/master/setup | bash -`
  * Set a password for the admira user when prompted.
  * When the script ends, just reboot the system with this command: `sudo reboot`
  * The system will start a desktop enviroment.
  * Right-click with the mouse and select: *Applications -> ADmira Player -> Install*
  * Follow the instructions and restart the system.
  * The system will start the ADmira Player register screen.
  * Register the player and add content using the [new.admira.mobi](https://new.admira.mobi) platform.

### Install Raspberry Pi OS Lite ###

* [How to install Raspberry Pi OS Lite](https://www.raspberrypi.org/documentation/installation/installing-images/README.md)

### Install Debian ###

  * Download [firmware-10.10.0-i386-netinst.iso](https://cdimage.debian.org/debian-cd/current/i386/iso-cd/debian-10.10.0-i386-netinst.iso) or [firmware-10.10.0-amd64-netinst.iso](https://cdimage.debian.org/debian-cd/current/amd64/iso-cd/debian-10.10.0-amd64-netinst.iso) and install it.
  
    * Tip: You can use the [balenaEtcher](https://www.balena.io/etcher/) tool in order to create a bootable USB flash drive with this image.
  * **IMPORTANT!!!** During the installation process you will be prompted to create a user. Please, do not use `admira` for such a user. Furthermore, select **only** these two packages groups when in the package selection step: `SSH server` and `standard system utilities`.
  * Now follow the steps described in the **How to install ADmira Player** section.
