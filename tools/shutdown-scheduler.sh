#!/bin/bash

# This program allows you to schedule simple shutdown commands into the crontab
#
# mmartinez@admira.com
# August 2019
#

crontab_file="/var/spool/cron/crontabs/admira"
signature="#_admira_crontab_scheduler_"

function cancel_info {
   zenity --info \
       	  --title="Canceled!" \
	  --width=350 \
	  --height=300 \
	  --text="The program was canceled.\n\nNo actions  will be scheduled.\n\n\nBye!"
}

function handle_result {
  case $? in
           0)
                  #echo "You selected $day_week"
                  ;;
           1)
                  cancel_info
                  exit 0
                  ;;
          -1)
                  #echo "An unexpected error has occurred."
                  exit 1
                  ;;
  esac
}

function show_current_action {
  current_action=$(awk '/_admira_crontab_scheduler_/{getline; print}' $crontab_file)
  minute=$(echo "$current_action" | cut -d' ' -f 1)
  hour=$(echo "$current_action" | cut -d' ' -f 2)
  day_week=$(echo "$current_action" | cut -d' ' -f 5)
  action=$(echo "$current_action" | cut -d' ' -f 7)
  zenity --question \
          --title="Action already scheduled!" \
          --width=350 \
          --height=300 \
          --text="You already have a scheduled action in your crontab:\n\n \
   Action: $action\n \
   Run at: $hour:$minute\n \
   Every:  $day_week\n\n \
Do you want to replace the current action with a new one ?" 

case $? in
         0)
                sed -ie '/_admira_crontab_scheduler_/,+2d' $crontab_file 
                ;;
         1)
                cancel_info
                exit 0
                ;;
        -1)
                echo "An unexpected error has occurred."
                exit 1
                ;;
esac
}


# main

if [ -f "$crontab_file" ]
then
  grep $signature $crontab_file && show_current_action
fi

action=$(zenity --list \
       --title="Set Action: step 1/5" \
       --width=400 \
       --height=300 \
       --window-icon="question"  \
       --column="Check" \
       --column="Action" \
       --column="Description" \
       FALSE  "poweroff" "Poweroff the system" \
       TRUE "reboot" "Restart the system" \
       FALSE "remove" "Removes any shceduled action" \
       --radiolist)

handle_result

if [ "$action" == "remove" ]
then
  sed -ie '/_admira_crontab_scheduler_/,+2d' $crontab_file
  zenity --info \
       --title="Remove scheduled actions" \
       --width=350 \
       --height=300 \
       --text="\nScheduled actions has been remoded from crontab."
  handle_result
  exit 0 
fi

day_week=$(zenity --list \
       --title="Choose frequency: step 2/5" \
       --width=400 \
       --height=300 \
       --window-icon="question"  \
       --separator="," \
       --column="Check" \
       --column="Day of the week" \
       --column="Description" \
       TRUE "SUN" "Every Sunday" \
       TRUE "MON" "Every Monday" \
       TRUE "TUE" "Every Tuesday" \
       TRUE "WED" "Every Wednesday" \
       TRUE "THU" "Every Thursday" \
       TRUE "FRI" "Every Friday" \
       TRUE "SAT" "Every Saturday" \
       --checklist)

handle_result

hour=$(zenity --scale \
       --title="Choose hour: step 3/5" \
       --width=400 \
       --height=300 \
       --window-icon="question"  \
       --text="Select the hour (0-23h) to perform the action" \
       --min-value=0 \
       --max-value=23)

handle_result

minute=$(zenity --scale \
       --title="Choose minute: step 4/5" \
       --width=400 \
       --height=300 \
       --window-icon="question"  \
       --text="Select the minute (0-59h) to perform the action" \
       --min-value=0 \
       --max-value=59)

handle_result

crontab_line="$minute $hour * * $day_week systemctl $action"

echo "$signature"  >> $crontab_file
echo "$crontab_line"  >> $crontab_file
chown admira $crontab_file
chmod 600 $crontab_file


zenity --info \
       --title="Congratulations!" \
       --width=350 \
       --height=300 \
       --text="\nThe following line has been added into:\n\n   /var/spool/cron/crontabs/admira\n\n$crontab_line"
