#!/usr/bin/env bash
#
# This script allows you to rotate the screen and make the chamges persistent
#
# Disable DPMS and prevent screen saver from blanking    
#   xset s off -dpms
#
# Allow remote reboot from admira platform
#   PATH=${PATH}:/usr/sbin
#
# Change screen orientation
#   xrandr -o <orientation>
#

function show_apply_permanent_msg {
  (
  for i in 5 10 15 20 25 30 35 40 45 50 55 60 65 70 75 80 85 90 95 100
  do
    echo $i
    sleep 0.5
  done
  ) | zenity --progress \
    --title="New screen settings" \
    --text="Reverting changes... Press cancel to mantain current settings ?" \
    --percentage=0 \
    --auto-close
  # Don't put any command here!!!  
  if [ "$?" = 1 ] ; then
    apply="yes"
  fi
}

function save_orientation {
user=$(whoami)
cat > ~/.xsessionrc <<EOF
xset s off -dpms
PATH=${PATH}:/usr/sbin
xrandr -o ${1}
EOF
chown $user.$user ~/.xsessionrc
}

function change_resolution {
  export DISPLAY=:0.0
  x_res=$1
  y_res=$2
  refresh=$3
  output=$(xrandr -q |grep ' connected'|awk '{print $1}')
  # Detect current orientation (.xsessionrc)
  . $screen_settings
  # Detect output (xrandr)
  output=$(xrandr -q |grep ' connected ' | awk '{print $1}')  
  # Get modeline for the resolution (cvt, gtf)
  modeline=$(gtf $x_res $y_res $refresh |grep Modeline | sed -e 's/.*Modeline \".*\"  //')
  # Add new modeline (xrandr)
  cat > $HOME/.fluxbox/custom_modeline <<EOF
xrandr --newmode "admira-ald" $modeline
xrandr --addmode $output "admira-ald" 
xrandr --output $output --mode "admira-ald"
EOF
  # Load new settings
  . $HOME/.fluxbox/custom_modeline
}

apply='no'
orientation='normal'
screen_settings='/home/admira/tools/screen_settings'

case $1 in
   normal)
     xrandr -o $1 
     show_apply_permanent_msg
     if [ "$apply" == "yes" ]; then 
        save_orientation $1
        echo "orientation='normal'" > $screen_settings
      else
        # load previous value
        . $screen_settings 
        xrandr -o $orientation
     fi
   ;;
   left)
     xrandr -o $1
     show_apply_permanent_msg
     if [ "$apply" == "yes" ]; then 
        save_orientation $1
        echo "orientation='left'" > $screen_settings
      else
        # load previous value
        . $screen_settings
        xrandr -o $orientation
     fi
   ;;
   right)
     xrandr -o $1
     show_apply_permanent_msg
     if [ "$apply" == "yes" ]; then 
        save_orientation $1
        echo "orientation='right'" > $screen_settings
      else
        # load previous value
        . $screen_settings
        xrandr -o $orientation
     fi
   ;;
   resolution)
     change_resolution $2 $3 $4
   ;;
   *)
     cat << EOF
usage: screen-manager.sh <normal|left|right|resolution> [x] [y] [refresh]

      normal : rotate to normal (not rotated at all)
        left : rotate letf
       right : rotate right
  resolution : auto detects the correct modeline for the provided resolution.
               Useful when your screen can't display the desktop automaticaly.
           x : Desired resolution for your screen.
           y : Desired vertical resolution for your screen.
     refresh : The refresh rate desired.

   example: screen-manager.sh resolution 1920 1080 60
   
EOF
     sleep 2
     exit 1
   ;;
esac
